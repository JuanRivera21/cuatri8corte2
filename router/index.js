import  express  from 'express';
import  json  from 'body-parser';
import { render } from 'ejs';

import alumnoDb from '../models/alumnos.js';

export const router = express.Router();

router.get('/',(req,res)=>{

    res.render('index',{titulo:"Mis Practicas js", nombre   :"Juan Rivera"})

})

router.post('/tabla',(req,res)=>{

    const params = {
        numero:req.body.numero
    }

    res.render('tabla',params);
})

router.get('/tabla',(req,res)=>{

    const params = {
        numero:req.query.numero
    }

    res.render('tabla',params);
})

router.post('/cotizacion',(req,res)=>{

    const params = {
        valor:req.body.valor,
        pinicial:req.body.pinicial,
        plazos:req.body.plazos,
        folio:req.body.folio,
        desc:req.body.desc

    }

    res.render('cotizacion',params);
})

router.get('/cotizacion',(req,res)=>{

    const params = {
        valor:req.query.valor,
        pinicial:req.query.pinicial,
        plazos:req.query.plazos,
        folio:req.query.folio,
        desc:req.query.desc
    }

    res.render('cotizacion',params);
})

let rows;

router.get('/alumnos',async(reg,res)=>{
    rows = await alumnoDb.mostrarTodos();
    res.render('alumnos',{reg:rows});
});

let params;

router.post('/alumnos',async(req,res)=>{
    try{
        params = {
            matricula:req.body.matricula,
            nombre:req.body.nombre,
            domicilio:req.body.domicilio,
            sexo:req.body.sexo,
            especialidad:req.body.especialidad
        }
       const res = await alumnoDb.insertar(params);
    }catch(error){
        console.error(error)
        res.status(400).send("Sucedio un error: " + error);
    }

    rows = await alumnoDb.mostrarTodos();
    res.render('alumnos',{reg:rows});
});


export default {router};